import React, { useState, useEffect } from "react";
import { initializeApp } from "firebase/app";
import { getStorage, ref, listAll, getDownloadURL } from "firebase/storage";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";

const ImageDisplay = () => {
  const [imageUrls, setImageUrls] = useState([]);

  useEffect(() => {
    const config = {
      apiKey: "AIzaSyB-fhYvIZbQIkEsBZCE2GF44d2Mh3skKks",
      authDomain: "ipsas-ens.net",
      projectId: "iotproject-15630",
      storageBucket: "gs://iotproject-15630.appspot.com",
      messagingSenderId: "YOUR_MESSAGING_SENDER_ID",
      appId: "YOUR_APP_ID",
    };

    const firebaseApp = initializeApp(config);
    const storage = getStorage(firebaseApp);
    const storageRef = ref(storage);
    listAll(storageRef)
      .then((result) => {
        const promises = result.items.map((item) => getDownloadURL(item));
        return Promise.all(promises);
      })
      .then((urls) => {
        setImageUrls(urls);
      })
      .catch((error) => {
        console.error("Error getting image URLs:", error);
      });
  }, []);

  return (
    <div>
      <h1>Images depuis Firebase</h1>
      <div
        style={{
          display: "grid",
          gridTemplateColumns: "repeat(auto-fill, minmax(300px, 1fr))",
          gap: "20px",
        }}
      >
        {imageUrls.map((data, index) => (
          <Card sx={{ maxWidth: 345, display: "grid", flexDirection: "row" }}>
            <CardActionArea>
              <CardMedia
                component="img"
                height="140"
                image={data}
                alt={`Image ${index + 1} depuis Firebase`}
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                  {`Image ${index + 1} depuis Firebase`}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        ))}
      </div>
    </div>
  );
};

export default ImageDisplay;
