import React, { useState } from "react";
import { getStorage, ref, uploadBytesResumable } from "firebase/storage";
import { initializeApp } from "firebase/app";
import LinearProgress from "@mui/material/LinearProgress";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";

const ImageUpload = () => {
  const [selectedImage, setSelectedImage] = useState(null);
  const [uploadProgress, setUploadProgress] = useState(0);

  const handleImageChange = (e) => {
    if (e.target.files && e.target.files[0]) {
      setSelectedImage(e.target.files[0]);
    }
  };

  const handleUpload = () => {
    if (selectedImage) {
      // Initialize Firebase
      const config = {
        apiKey: "AIzaSyB-fhYvIZbQIkEsBZCE2GF44d2Mh3skKks",
        authDomain: "ipsas-ens.net",
        projectId: "iotproject-15630",
        storageBucket: "gs://iotproject-15630.appspot.com",
        messagingSenderId: "YOUR_MESSAGING_SENDER_ID",
        appId: "YOUR_APP_ID",
      };

      const firebaseApp = initializeApp(config);

      // Reference to the root of Firebase Storage
      const storage = getStorage(firebaseApp);

      // Reference to the root of Firebase Storage
      const storageRef = ref(storage);

      // Reference to the selected image in Firebase Storage
      const imageRef = ref(storageRef, selectedImage.name);

      // Upload the image to Firebase Storage
      const uploadTask = uploadBytesResumable(imageRef, selectedImage);

      // Set up progress listener
      uploadTask.on(
        "state_changed",
        (snapshot) => {
          const progress =
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          setUploadProgress(progress);
        },
        (error) => {
          console.error("Error uploading image:", error);
        },
        () => {
          console.log("Image uploaded successfully!");
          window.location.reload();
          setSelectedImage(null);
          setUploadProgress(0);
        }
      );
    }
  };

  return (
    <div>
      <h1>Upload d'image vers Firebase</h1>
      <input type="file" accept="image/*" onChange={handleImageChange} />
      <Button
        disabled={!selectedImage}
        variant="contained"
        onClick={handleUpload}
      >
        Envoyer l'image
      </Button>
      <Box sx={{ display: "flex", alignItems: "center" }}>
        <Box sx={{ width: "100%", mr: 50 }}>
          <LinearProgress variant="determinate" value={uploadProgress} />
        </Box>
        <Box sx={{ minWidth: 35 }}>
          <Typography variant="body2" color="text.secondary">{`${Math.round(
            uploadProgress
          )}%`}</Typography>
        </Box>
      </Box>
    </div>
  );
};

export default ImageUpload;
