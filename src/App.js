// App.js
import React from 'react';
import ImageDisplay from './ImageDisplay';
import ImageUpload from './ImageUpload';

function App() {
  return (
    <div className="App">
      <ImageUpload />
      <ImageDisplay />
    </div>
  );
}

export default App;
